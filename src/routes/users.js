const express = require('express');
const router = express.Router();
const Session = require('../models/session');
const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../config/keys');

// Middleware Function
const getUserById = async (request, response, next) => {
  try {
    user = await User.findById(request.params.id);
    if (user == null) {
      return response.status(404).json({ message: 'User not found.'})
    }
  } catch(err){
    return response.status(500).json({ message: err.message })
  }

  response.user = user;
  next();
}

// Get Users
router.get('/', async (request, response) => {
  try {
    const users = await User.find();
    response.json(users);
  } catch (err) {
    response.status(500).json({ message: err.message });
  }
});

// Get a User
router.get('/:id', getUserById, async (request, response) => {
  try {
    await response.json(response.user);
  } catch (err) {
    response.status(500).json({ message: err.message });
  }
});

// Create a User
router.post('/', async (request, response) => {
  const { name, email, password, username } = request.body;
  const newPassword = bcrypt.hashSync(password, 10);
  const user = new User({
    name,
    email,
    password: newPassword,
    photoURI: '',
    username,
    date: Date.now()
  });

  try {
    const existingUser = await User.findOne({ email });
    if (!existingUser) {
      const newUser = await user.save();

      // Create a new session
      const payload = { id: newUser.id };
      const token = jwt.sign(payload, jwtSecret);

      response.cookie('access_token', token, {
        maxAge: 3600,
        httpOnly: true,
        // secure: true
      });

      const session = new Session({
        userId: newUser.id,
        token,
        createdAt: Date.now()
      });

      try {
        const existingSession = await Session.findOne({ userId: newUser.id });
        if (!existingSession) {
          const newSession = await session.save();
          response.session = newSession;
        } else {
          response.send(existingSession);
        }
      } catch(err) {
        response.status(500).json({ message: err.message });
      }

      response.status(201).json({ newUser, session: response.session });
    } else {
      response.json({ message: 'That email already exists.' });
    }
  } catch (err) {
    response.status(500).json({ message: err.message });
  }
});

// Validate a User (Login)
router.post('/isValid', async (request, response) => {
  const { email, password } = request.body;
  const user = await User.findOne({ email });

  if (user)
    response.json({ isValid: bcrypt.compareSync(password, user.password) });
  else
    response.json(undefined);
});

// Delete a User
router.delete('/:id', getUserById, async (request, response) => {
  try {
    await response.user.remove();
    response.json({ message: `User @${response.user.username} deleted.` });
  } catch(err) {
    response.status(500).json({ message: err.message });
  }
});

module.exports = router;
