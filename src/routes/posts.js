const express = require('express');
const router = express.Router();
const Post = require('../models/post');
const auth = require('../middleware/auth');

// Middleware Function
const getPostById = async (request, response, next) => {
  try {
    post = await Post.findById(request.params.id);
    if (post == null) {
      return response.status(404).json({ message: 'Post not found.'})
    }
  } catch(err){
    return response.status(500).json({ message: err.message })
  }

  response.post = post;
  next();
}

// Get Posts
router.get('/', async (request, response) => {
  try {
    const posts = await Post.find();
    response.json(posts);
  } catch (err) {
    response.status(500).json({ message: err.message });
  }
});

// Get a Post
router.get('/:id', getPostById, async (request, response) => {
  try {
    await response.json(response.post);
  } catch (err) {
    response.status(500).json({ message: err.message });
  }
});

// Check if any posts by a given user exist
router.get('/check/:userId', async (request, response) => {
  const userId = request.params.userId;
  const post = await Post.findOne({ userId });
  try{
    response.json(post);
  }catch(err){
    response.status(500).json({ message: err.message });
  }
});

// Create a Post
router.post('/', auth, async (request, response) => {
  const { userId, content, icons, comments } = request.body;
  const post = new Post({
    userId,
    content,
    icons,
    comments
  });

  try {
    if (userId !== 0) {
      const existingPost = await Post.findOne({ userId, content });
      if (!existingPost) {
        const newPost = await post.save();
        response.status(201).json(newPost);
      } else {
        response.json({ message: 'Spam Warning: You\'ve already made a post like this one.' });
      }
    } else {
      response.json({ message: 'Error: Please try logging in again.' });
    }
  } catch (err) {
    response.status(500).json({ message: err.message });
  }
});

// Like OR Dislike a Post
router.put('/:id/like', auth, async (request, response) => {
  try {
    Post.findByIdAndUpdate({ _id: request.params.id }, request.body)
      .then(() => Post.findOne({ _id: request.params.id }))
      .then(post => response.send(post));
  } catch (err) {
    response.status(500).json({ message: err.message });
  }
});

// Create a Post Comment
router.put('/:id/comment', auth, async (request, response) => {
  try {
    Post.findByIdAndUpdate({ _id: request.params.id }, request.body)
      .then(() => Post.findOne({ _id: request.params.id }))
      .then(post => response.send(post.comments));
  } catch (err) {
    response.status(500).json({ message: err.message });
  }
});

// Delete a post
router.delete('/:id', auth, getPostById, async (request, response) => {
  try {
    await response.post.remove();
    response.json({ message: 'Post deleted' });
  } catch(err) {
    response.status(500).json({ message: err.message });
  }
});

module.exports = router;
