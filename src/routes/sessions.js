const express = require('express');
const router = express.Router();
const Session = require('../models/session');
const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../config/keys');

// Create a Session (Generate and Store Token)
router.post('/auth/login', async (request, response) => {
  const { email, password } = request.body;
  const user = await User.findOne({ email });

  if (user) {
    if (bcrypt.compareSync(password, user.password)) {
      const payload = { id: user.id };
      const token = jwt.sign(payload, jwtSecret);

      response.cookie('access_token', token, {
        maxAge: 3600,
        httpOnly: true,
        // secure: true
      });

      const session = new Session({
        userId: user.id,
        token
      });
      
      try {
        const existingSession = await Session.findOne({ userId: user.id });
        if (!existingSession) {
          const newSession = await session.save();
          response.status(201).json(newSession);
        } else {
          response.send(existingSession);
        }
      } catch(err) {
        response.status(500).json({ message: err.message });
      }
    } else {
      response.status(400).json({ message: 'Invalid credentials.' });
    }
  } else {
    response.status(400).json({ message: 'Invalid credentials.' });
  }
});

// Delete a Session (Logout)
router.delete('/auth/logout', async (request, response) => {
  try {
    const session = await Session.findOne({ userId: request.body.id });
    await session.remove();
    response.status(201).json({ message: 'Post removed successfully.' });
  } catch(err) {
    response.status(500).json({ message: err.message });
  }
});

module.exports = router;
