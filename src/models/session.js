const mongoose = require('mongoose');

const sessionSchema = new mongoose.Schema({
    userId: {
      type: mongoose.Schema.ObjectId,
      required: true
    },
    token: {
      type: String,
      required: true
    },
    createdAt: {
      type: Date,
      expires: '1h',
      default: Date.now
    }
});

const Session = mongoose.model('Session', sessionSchema);

module.exports = Session;
