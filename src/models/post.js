const mongoose = require('mongoose');

const postSchema = new mongoose.Schema({
    userId: String,
    content: String,
    icons: Object,
    comments: Array,
    date: {
        type: Date,
        default: Date.now
    }
});

const Post = mongoose.model('Post', postSchema);

module.exports = Post;
