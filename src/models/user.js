const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: String,
    username: String,
    email: String,
    password: String,
    photoURI: String,
    date: Date
});

const User = mongoose.model('User', userSchema);

module.exports = User;
