const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const server = express();
const PORT = 4000;

// Routes
const postsRouter = require('./routes/posts');
const sessionsRouter = require('./routes/sessions');
const usersRouter = require('./routes/users');

server.use(express.json());
server.use(express.urlencoded({ extended: false }));

mongoose.connect('mongodb://localhost/bookface', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
});

const db = mongoose.connection;
db.once('open', async () => {
   console.log('connected to mongodb');
});

server.use(
    cors({
        origin: 'http://localhost:3000',
        credentials: true,
    })
);

// Use Routes
server.use('/api/posts', postsRouter);
server.use('/api/sessions', sessionsRouter);
server.use('/api/users', usersRouter);

server.listen(PORT, () => console.log('server is up'));
