const Post = require('../models/post');
const Session = require('../models/session');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../config/keys');

module.exports = async (request, response, next) => {
  // Get token
  let token;
  let session;

  if (request.params.length) {
    const { id } = request.params;
    const { userId } = post = await Post.findOne({ id });
    session = await Session.findOne({ userId });
  } else if (request.body.userId !== null) {
    session = await Session.findOne({ userId: request.body.userId });
  } else {
    const user = await User.findOne({ email: request.body.email });
    session = await Session.findOne({ userId: user.userId });
  }

  if (!session)
    return response.status(401).json({ authMessage: 'No token. Authorization denied.' });
  else {
    if (session.token)
      token = session.token;
    else
      return response.status(401).json({ authMessage: 'No token. Authorization denied.' });
  }

  // Verify token
  try {
    const decoded = jwt.verify(token, jwtSecret);
    request.user = decoded.user;
    next();
  } catch(err) {
    response.status(401).json({ authMessage: 'Token is not valid.' });
  }
}
